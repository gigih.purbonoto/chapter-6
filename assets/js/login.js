// untuk menangkap inputan dari FE
function getDataFormLogin() {
    // get Value via DOM
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    // console.log({username, password})
    return { username, password }
}

async function onSubmit() {
    const formData = getDataFormLogin()

    // kita kirimkan data yg sudah ditangkap ke BE
    // data yang dikirimkan ke BE menggunakan method POST
    // GET untuk menampilkan kalo POST mengirimkan
    // kita kasih tau BE bahwa data yg kita kirim berupa JSON
    // kita tampung ke dalam body data yg ditangkap tadi melalui func getDataFormLogin yg sudah di define formData 
    const res = await fetch('http://localhost:3000/users', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: { 'Content-Type': 'application/json' },
    })

    // kita tangkap hasil respon dari BE
    const { status } = await res.json();


    switch (status) {
        case "logged-in": {
            const close = document.getElementById('btnClose').click()
            document.getElementById('admin').classList.remove('d-none')
            const loginName = document.getElementById('loginName')
            loginName.innerHTML = formData.username
            break;
        }

        case "invalid-password": {
            const pass = document.getElementById('password')
            pass.classList.add('is-invalid')
            const alertPass = document.getElementById('alertPassword')
            alertPass.classList.add('text-danger')
            alertPass.classList.remove('d-none')
            alertPass.innerHTML = status
            break;
        }

        case "no-username": {
            const userElement = document.getElementById('username')
            userElement.classList.add('is-invalid')
            const alertUsername = document.getElementById('alertUsername')
            alertUsername.classList.add('text-danger')
            alertUsername.classList.remove('d-none')
            alertUsername.innerHTML = status
            break;
        }

    }
}