"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.UserBiodata, {
        foreignKey: "user_game_id",
        sourceKey: "id",
      });

      this.hasMany(models.UserHistory, {
        foreignKey: "user_game_id",
        sourceKey: "id",
      });
    }
  }
  UserGame.init(
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "UserGame",
    }
  );
  return UserGame;
};

// DDL (Data Definition Language)
