"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "UserGames",
      [
        {
          username: "username1",
          password: "123456",
          createdAt: "2022-07-07",
          updatedAt: "2022-07-07",
        },
        {
          username: "username2",
          password: "123456789",
          createdAt: "2022-07-07",
          updatedAt: "2022-07-07",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
