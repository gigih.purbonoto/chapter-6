// import module express
const express = require("express");
// memakai func atau metodh yg ada didalam Express JS
const app = express();
// define untuk Port
const port = 3000;
// define atau import file JSON
let users = require("./users.json");

const { UserGame, UserBiodata, UserHistory } = require("./models");

app.set("view engine", "ejs");

// regis untuk root folder assets
app.use(express.static(__dirname + "/assets"));
// regis untuk root folder node module
app.use(express.static(__dirname + "/node_modules"));
// untuk menampilkan data berupa JSON (default Object)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// buat Routing / URL untuk index.html
app.get("/", (req, res) => {
  res.status(200).render("index");
});

app.get("/suit", (req, res) => {
  res.status(200).render("suit");
});

app.get("/admin", async (req, res) => {
  //   fetching data dari Database untuk table UserGames
  const userGame = await UserGame.findAll({
    order: [["id", "ASC"]],
  });
  res.status(200).render("admin_user", { userGame });
});

app.get("/userCreate", (req, res) => {
  res.status(200).render("admin_user_create");
});

app.post("/userCreate", async (req, res) => {
  // tangkap dulu data yg dikirm dari FE
  const { username, password, name, company, description } = req.body;

  //   kita insert ke masing2 table yg bersangkutan
  const userGame = await UserGame.create({ username, password });
  await UserBiodata.create({
    name,
    company,
    description,
    user_game_id: userGame.id,
  });
  res.status(200).redirect("/admin");
});

app.get("/userDetail/:id", async (req, res) => {
  const userGameDetail = await UserGame.findOne({
    where: { id: +req.params.id },
    include: UserBiodata,
  });
  const userHistory = await UserHistory.findAll({
    where: { user_game_id: +req.params.id },
  });
  res.status(200).render("admin_user_view", { userGameDetail, userHistory });
});

app.get("/userUpdate/:id", async (req, res) => {
  const userUpdate = await UserGame.findOne({
    where: { id: +req.params.id },
    include: UserBiodata,
  });
  res.status(200).render("admin_user_edit", { userUpdate });
});

app.post("/userUpdate/:id", async (req, res) => {
  // tangkap dulu data yg dikirm dari FE
  const { username, password, name, company, description } = req.body;

  //   update untuk Table UserGame dengan id yg sama
  await UserGame.update(
    { username, password },
    {
      where: { id: +req.params.id },
    }
  );
  await UserBiodata.update(
    { name, company, description },
    {
      where: { user_game_id: +req.params.id },
    }
  );
  res.status(200).redirect("/admin");
});

app.get("/userDelete/:id", async (req, res) => {
  await UserGame.destroy({
    where: { id: +req.params.id },
  });
  await UserBiodata.destroy({
    where: { user_game_id: +req.params.id },
  });
  res.status(200).redirect("/admin");
});

app.get("/userHistoryCreate", async (req, res) => {
  const userGame = await UserGame.findAll({
    order: [["id", "ASC"]],
  });
  res.status(200).render("admin_user_history_create", { userGame });
});

app.post("/userHistoryCreate", async (req, res) => {
  // tangkap dulu data yg dikirm dari FE
  const { user_game_id, game_name, score, duration } = req.body;

  //   kita insert ke masing2 table yg bersangkutan
  await UserHistory.create({ user_game_id, game_name, score, duration });

  res.status(200).redirect("/admin");
});

app.get("/userHistoryUpdate/:id", async (req, res) => {
  const userHistoryUpdate = await UserHistory.findOne({
    where: { id: +req.params.id },
  });
  const userGame = await UserGame.findAll({
    order: [["id", "ASC"]],
  });
  res
    .status(200)
    .render("admin_user_history_edit", { userHistoryUpdate, userGame });
});

app.post("/userHistoryUpdate/:id", async (req, res) => {
  // tangkap dulu data yg dikirm dari FE
  const { user_game_id, game_name, score, duration } = req.body;

  //   update untuk Table UserGame dengan id yg sama
  await UserHistory.update(
    { user_game_id, game_name, score, duration },
    {
      where: { id: +req.params.id },
    }
  );
  res.status(200).redirect("/userDetail/" + user_game_id);
});

app.get("/userHistoryDelete/:id", async (req, res) => {
  const userHistory = await UserHistory.findOne({
    where: { id: +req.params.id },
  });
  await UserHistory.destroy({
    where: { id: +req.params.id },
  });
  res.status(200).redirect("/userDetail/" + userHistory.user_game_id);
});

app.post("/users", (req, res) => {
  // kita tangkap data yg dikirimkan oleh FE
  const { username, password } = req.body;
  // console.log({username, password})
  // setelah data ditangkap kemudian kita cek dengan data yg ada di BE (users.json)

  // kita cek dulu untuk username, ada atau tidak antara data yg dikirim FE dengan BE
  // == untuk cek datanya === sekaligus cek type data
  const user = users.find((item) => item.username == username);
  // kita lakukan cek lagi untuk passwordnya
  if (user) {
    // jika passwordnya ada atau sama
    if (user.password == password) {
      // kita kirim balik ke FE bahwa data yg dikitimkan sesuai / cocok
      res.status(200).json((userMerged = { status: "logged-in", ...user }));
      // console.log('anda berhasil login')
    } else {
      res.status(200).json({ status: "invalid-password" });
      // console.log('invalid-password')
    }
  } else {
    res.status(200).json({ status: "no-username" });
    // console.log('username tidak ada, silahkan daftar')
  }
});

app.get("/api/users", (req, res) => {
  res.status(200).json(users);
});

app.listen(port, () => {
  console.log("Server runing on Port:", port);
});
